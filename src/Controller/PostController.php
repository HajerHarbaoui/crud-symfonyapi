<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PostController extends AbstractController
{
    /**
     * @Route("/api/post", name="post", methods={"GET"})
     */
    public function index(PostRepository $postRepository)
    {
        $posts = $postRepository->findAll();
//        $json = $serializer->serialize($posts,'json',
//            ['groups' => 'posts']);
//        $response = new JsonResponse($json,200,[],true);
        $response = $this->json($posts, 200, [], ['groups' => 'posts']);
        return $response;
    }


    /**
     * @Route("/api/post", name="storePost", methods={"POST"})
     */
    public function store(Request $request, EntityManagerInterface $em,
                          SerializerInterface $serializer)
    {

        $data = $request->getContent();
        $post = $serializer->deserialize($data, Post::class, 'json');

        $em->persist($post);
        $em->flush();
        return new JsonResponse($post, Response::HTTP_OK);

    }

    /**
     * @Route("/api/post/show/{id}", name="show", methods={"GET"})
     */
    public function show($id, SerializerInterface $serializer, PostRepository $postRepository)
    {
        $id = 1;
        $post = $postRepository->find($id);
        $data = $serializer->serialize($post, 'json',
            ['groups' => 'posts']);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/edit/{id}",methods={"PUT"})
     */
    public function editPost(Request $request, $id, PostRepository $postRepository
        , EntityManagerInterface $entityManager)
    {
        $post = $postRepository->find($id);
        $data = json_decode($request->getContent(), true);
        $post->setTitle($data["title"]);
        $post->setContent($data["content"]);
        $entityManager->persist($post);
        $entityManager->flush();
        $response = $this->json($post, 200, [], ['groups' => 'posts']);
        return $response;
    }

    /**
     * @Route("/delete/{id}",methods={"DELETE"})
     */
    public function removePerson($id, PostRepository $postRepository
        , EntityManagerInterface $entityManager)
    {
        $post = $postRepository->find($id);
        $entityManager->remove($post);
        $entityManager->flush();
        return new JsonResponse($post, Response::HTTP_OK);
    }
}
